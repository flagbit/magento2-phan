# Magento2 Phan
Magento2 Phan integrates the [Phan static code analyzer](https://github.com/phan/phan) into a magento2 project.
In the future this module will provide phan plugins for magento2 code analysis.

### Installation
##### Add this composer.json to the root folder of your project
This is not required you can also add phan without this repository, but we will provide phan plugins in this path
```
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://bitbucket.org/flagbit/magento2-phan"
    }
  ],
  "require": {
    "composer/installers": "~1.0",
    "oomphinc/composer-installers-extender": "^1.1",
    "roave/security-advisories": "dev-master",
    "flagbit/magento2-phan": "dev-master"
  },
  "extra": {
    "installer-types": ["magento2-phan"],
    "installer-paths": {
      ".phan/magento2": ["type:magento2-phan"]
    }
  }
}
```
##### Execute composer install in root folder
```
composer update --ignore-platform-reqs  
```
##### Add a config.php to 
```
.phan/config.php
```
Or copy and rename [example config file](https://bitbucket.org/flagbit/magento2-phan/raw/da1bab258fb02030402cec4809a9b1f6a35ae145/example/mage2.config.php).
##### Download phan.phar
into the **root folder** of the project
```
curl -L https://github.com/phan/phan/releases/download/0.11.0/phan.phar -o phan.phar;
```
##### Set executable to phan.phar
```
chmod +x phan.phar
```
##### Test the phan.phar with help command
```
php phan.phar --help
```
##### Update the .gitignore file in the project root
```
vendor
.phan/magento2
!.phan/config.php
phan.phar
```

### Execute code analysis
Command to execute static code analysis from project root folder for magento2
```
php phan.phar -i --color
```
The option -i ignore undeclared functions and classes
